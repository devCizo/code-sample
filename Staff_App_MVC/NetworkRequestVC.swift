//
//  NetworkRequestVC.swift

//
//  Created by apple on 05/10/22.
//  Copyright © 2022 Cizo Tech. All rights reserved.
//

import UIKit
import BadgeSwift

class NetworkRequestVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var networkRequestTableView: UITableView!
    @IBOutlet weak var badgeView: BadgeSwift!
    @IBOutlet weak var currentLabel: UILabel!
    
    var networkRequestArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set title for navigation bar
        self.title = StringConstant.navigationBarTitle
        
        //register cell
        self.networkRequestTableView.register(UINib(nibName: XIBConstant.networkRequestCell, bundle: nil), forCellReuseIdentifier: XIBConstant.networkRequestCell)
                
        //fetch network request data from server
        self.fetchNetworkRequest()
        
        // Do any additional setup after loading the view.
    }
    
    
    func fetchNetworkRequest(){
        if let path = Bundle.main.path(forResource: ServerConstant.dummyData, ofType: ServerConstant.json) {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResponse = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResponse as? NSDictionary , let jsonData = jsonResult[ServerConstant.data] as? NSArray{
                    self.networkRequestArray.removeAllObjects()
                    for j in jsonData{
                        let model = NetworkRequest(dict: j as! NSDictionary)
                        self.networkRequestArray.add(model)
                    }
                    
                    if self.networkRequestArray.count > 0{
                        self.badgeView.text = String(self.networkRequestArray.count)
                        self.badgeView.font = UIFont.boldSystemFont(ofSize: 15)
                        self.badgeView.textColor = UIColor.white
                        self.networkRequestTableView.delegate = self
                        self.networkRequestTableView.dataSource = self
                        self.networkRequestTableView.reloadData()
                    }else{
                        let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: networkRequestTableView.bounds.size.width, height: networkRequestTableView.bounds.size.height))
                        noDataLabel.text = StringConstant.noDataAvailable
                        noDataLabel.textColor = UIColor.black
                        noDataLabel.textAlignment = .center
                        networkRequestTableView.backgroundView  = noDataLabel
                        self.currentLabel.isHidden = true
                        self.badgeView.isHidden = true
                    }
                }
            } catch {
                // handle error
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.networkRequestArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XIBConstant.networkRequestCell, for: indexPath) as! NetworkRequestCell
        
        let model = self.networkRequestArray.object(at: indexPath.row) as! NetworkRequest
        
        cell.profileImage.image = UIImage.init(named: model.profile)
        cell.nameLbl.text = model.name
        cell.phoneNumberLbl.text = model.mobile_number
        cell.designationLbl.text = model.designation
        cell.availabilityLbl.text = model.availibility
        cell.subTotalLbl.text = model.subtotal
        cell.detailsLbl.text = model.details
        
        cell.acceptView.isHidden = model.isAccepted == "1" ? false : true
        cell.acceptView.layer.borderWidth = 1.5
        cell.acceptView.layer.cornerRadius = 10
        cell.acceptView.layer.borderColor = hexStringToUIColor(hex: StringConstant.greenColorCode).cgColor
        cell.acceptView.clipsToBounds = true
        
        cell.rejectView.isHidden = model.isRejected == "1" ? false : true
        cell.rejectView.layer.borderWidth = 1.5
        cell.rejectView.layer.cornerRadius = 10
        cell.rejectView.layer.borderColor = hexStringToUIColor(hex: StringConstant.redColorCode).cgColor
        cell.rejectView.clipsToBounds = true
        
        cell.availableView.isHidden = model.isAvailable == "1" ? false : true
        cell.availableView.layer.cornerRadius = 10
        
        cell.showView.isHidden = model.isShow == "1" ? false : true
        cell.showView.layer.borderWidth = 1.5
        cell.showView.layer.cornerRadius = 10
        cell.showView.layer.borderColor = hexStringToUIColor(hex: StringConstant.blueColorCode).cgColor
        cell.showView.clipsToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = PopUpVC(nibName: XIBConstant.popUpVC, bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.isModalInPresentation = true
        self.present(vc, animated: true, completion: nil)
    }
    
    
}
