

import UIKit

class AssignmentListVC: BaseVC {

    
    @IBOutlet weak var NoDataStack: UIStackView! //Display when data
    @IBOutlet weak var imgGradiant: UIImageView! //background gradiant show when data list loaded
    var isGradiantAddedInImage = false // flag for adding background gradiant one when screen apper
    
    @IBOutlet weak var tblAssignment: UITableView!
    var selectedAssignment = -1
    
    var assignmentListDataModel:AssignmentListDataModel? // Model containing data
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblAssignment.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
        tblAssignment.rowHeight = UITableView.automaticDimension
        tblAssignment.delegate = self
        tblAssignment.dataSource = self
        tblAssignment.reloadData()
        
        imgGradiant.isHidden = true
        tblAssignment.isHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.imgGradiant.isHidden = false
            strongSelf.tblAssignment.isHidden = false
            
            strongSelf.NoDataStack.isHidden = true

        })
        
        // Get data from backend
        let v = AssignmentListViewModel()
        v.getAllData { [weak self] in
            self?.assignmentListDataModel = v.assignmentListDataModel
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !isGradiantAddedInImage
        {
            isGradiantAddedInImage = true
            setBackgroundGradiant(img: imgGradiant)
        }
    }
}


extension AssignmentListVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return assignmentListDataModel?.response.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if let lblDay = cell.viewWithTag(1) as? UILabel,let lblTime = cell.viewWithTag(2) as? UILabel,let lblHr = cell.viewWithTag(3) as? UILabel
        {
            lblDay.text = assignmentListDataModel?.response[indexPath.row].day
            lblTime.text = assignmentListDataModel?.response[indexPath.row].time
            lblHr.text = assignmentListDataModel?.response[indexPath.row].hours
        }
        
        if let vw = cell.viewWithTag(4)
        {
            vw.layer.borderWidth = 0
            if selectedAssignment == indexPath.row
            {
                vw.layer.borderWidth = 1.5
            }
        }
        
        return cell
    }  
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedAssignment == indexPath.row
        {
            selectedAssignment = -1
        }
        else
        {
            selectedAssignment = indexPath.row
        }
        tableView.reloadData()
    }
    
}
