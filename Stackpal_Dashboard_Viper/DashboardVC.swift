import UIKit



class DashboardVC: UIViewController {

    @IBOutlet weak var mainScrollView: UIScrollView! //main container scrollview
    @IBOutlet weak var colStockHighlight: UICollectionView!// top stock highlight view
    @IBOutlet weak var tblTopSelling: UITableView! // list for top selling product
    @IBOutlet weak var CSTbltopSellingHeight: NSLayoutConstraint!
    @IBOutlet weak var btnFilter: UIButton!
    var topSellingArr:[NSDictionary] = []
    @IBOutlet weak var lblNoTopSellingProductMessage: UILabel! // message if no top selling product
    
    var grossSales = "0" // for top gross sales
    var netProfit = "0" // for net profit
    var transactions = "0" // for all transactions
    var activeInventory = "0" // for all active inventory
    var lowInentory = "0" // for low inventory
    var outOfStock = "0" // for out of stock
    
    
    var needToCallAPI = false // check for if new data available
    let refreshControl = UIRefreshControl() // for pull to refresh

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblNoTopSellingProductMessage.isHidden = true
        self.lblNoTopSellingProductMessage.text = "There are no products to show here."
        
        // load all data need in further app
        getPreRequiredData()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.callAPIForGetDashboardData(loader:)), for: .valueChanged)
        mainScrollView.addSubview(refreshControl)
        
        colStockHighlight.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        colStockHighlight.delegate = self
        colStockHighlight.dataSource = self
        colStockHighlight.reloadData()
        
        tblTopSelling.rowHeight = UITableView.automaticDimension
        tblTopSelling.registerCellNib(TopSellingTableCell.self)
        tblTopSelling.delegate = self
        tblTopSelling.dataSource = self
        tblTopSelling.reloadData()
                
        callAPIForGetDashboardData(loader: true)
        
        NOTIFICATIONCENTER.addObserver(self, selector: #selector(self.checkForPush), name: .onPushReceive, object: nil)
        checkForPush()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if !needToCallAPI
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: { [weak self] in
                guard let strongSelf = self else { return }
                
                strongSelf.needToCallAPI = true
            })
        }
        else
        {
            callAPIForGetDashboardData(loader: false)
        }
        
    }
    
    // handle if new push arrive
    @objc func checkForPush()
    {
        if pushDict != nil
        {
            btnNotification(UIButton())
        }
    }

    
    // for calculate top selling list height
    func calculateHeightForTopSelling()
    {
        CSTbltopSellingHeight.constant = CGFloat.greatestFiniteMagnitude
        tblTopSelling.reloadData()
        tblTopSelling.layoutIfNeeded()
        CSTbltopSellingHeight.constant = tblTopSelling.contentSize.height
    }
    
    // api for get dashboard data
    @objc func callAPIForGetDashboardData(loader:Bool)
    {
        if loader
        {
            showLoader()
        }
        
        var url = API_DASHBOARD
        if selectedFilterIndex != -1
        {
            url += "?\(KEY.fromDate)=\(filterStartDate)&\(KEY.toDate)=\(filterEndDate)"
        }
        APICall.call_API_With_GET_METHOD(url: url, reqType: 1) { dict in
            self.grossSales = "0"
            self.netProfit = "0"
            self.transcations = "0"
            self.activeInventory = "0"
            self.lowInentory = "0"
            self.outOfStock = "0"
            self.topSellingArr.removeAll()
            self.refreshControl.endRefreshing()
            if loader
            {
                hideLoader()
            }
            if let success = dict.value(forKey: SUCCESS) as? Bool,success == true
            {
                if let dashDict = dict.value(forKey: KEY.dashBoardResponse) as? NSDictionary
                {
                    if let v = dashDict.value(forKey: KEY.sales),"\(v)".isValidString()
                    {
                        self.grossSales = "\(v)"
                    }
                    if let v = dashDict.value(forKey: KEY.netProfit),"\(v)".isValidString()
                    {
                        self.netProfit = "\(v)"
                    }
                    if let v = dashDict.value(forKey: KEY.transaction),"\(v)".isValidString()
                    {
                        self.transcations = "\(v)"
                    }
                    if let v = dashDict.value(forKey: KEY.activeInventory),"\(v)".isValidString()
                    {
                        self.activeInventory = "\(v)"
                    }
                    if let v = dashDict.value(forKey: KEY.lowInventory),"\(v)".isValidString()
                    {
                        self.lowInentory = "\(v)"
                    }
                    if let v = dashDict.value(forKey: KEY.outOfStock),"\(v)".isValidString()
                    {
                        self.outOfStock = "\(v)"
                    }
                    if let v = dashDict.value(forKey: KEY.bestSeller) as? [NSDictionary]
                    {
                        self.topSellingArr = v
                    }
                }
            }
            else
            {
                
            }
            
            if self.topSellingArr.count == 0
            {
                self.lblNoTopSellingProductMessage.isHidden = false
            }
            else
            {
                self.lblNoTopSellingProductMessage.isHidden = true
            }
            
            self.colStockHighlight.reloadData()
            self.tblTopSelling.reloadData()
            self.calculateHeightForTopSelling()
        }
    }
    
    
    @IBAction func btnViewAllTopSelling(_ sender: UIButton) {
        let vc = UIStoryboard.Dashboard.instantiateViewController(withIdentifier: "TopSellingProductVC") as! TopSellingProductVC
        vc.topSellingArr = topSellingArr
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNotification(_ sender: UIButton) {
        let vc = UIStoryboard.Dashboard.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnFilter(_ sender: UIButton) {
        let vc = UIStoryboard.Dashboard.instantiateViewController(withIdentifier: "FilterOptionSelectVC") as! FilterOptionSelectVC
        vc.delegate = self
        AppDelegate.shared.window!.rootViewController!.present(vc, animated: true, completion: nil)
    }
    
    // get pre required data and save them
    func getPreRequiredData()
    {
        var str = getDictKeychain(key: KEY.categories)
        if let arr = str.value(forKey: KEY.categories) as? [NSDictionary]
        {
            categoriesListArrGlobal = arr
        }
        getCategoryListGlobal {
            
        }
        
        str = getDictKeychain(key: KEY.sizes)
        if let arr = str.value(forKey: KEY.sizes) as? [NSDictionary]
        {
            sizesListArrGlobal = arr
        }
        getSizesListGlobal {
            
        }
        
        str = getDictKeychain(key: KEY.priceTypes)
        if let arr = str.value(forKey: KEY.priceTypes) as? [NSDictionary]
        {
            priceTypeListArrGlobal = arr
        }
        getPriceTypeListGlobal {
            
        }
        
        str = getDictKeychain(key: KEY.tax)
        if let arr = str.value(forKey: KEY.tax) as? [NSDictionary]
        {
            taxListArrGlobal = arr
        }
        getTaxListGlobal {
            
        }
        
        str = getDictKeychain(key: KEY.taxTypes)
        if let arr = str.value(forKey: KEY.taxTypes) as? [NSDictionary]
        {
            taxTypeListArrGlobal = arr
        }
        getTaxTypeListGlobal {
            
        }
        
    }

}

// Top selling product list
extension DashboardVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return min(5, topSellingArr.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopSellingTableCell", for: indexPath) as! TopSellingTableCell
        let dict = topSellingArr[indexPath.row]
        
        cell.lblTitle.text = string(dict, KEY.name)
        cell.lblAmtSold.text = "$" + string(dict, KEY.sales).toDecimalWithTwoDecimal()
        cell.lblQSold.text = string(dict, KEY.qtySold)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}


// current stock data list
extension DashboardVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        if let lblTitle = cell.viewWithTag(1) as? UILabel,let img = cell.viewWithTag(2) as? UIImageView,let lblAmt = cell.viewWithTag(3) as? UILabel
        {
            if indexPath.row == 0 {
                lblTitle.text = "Gross Sales"
                lblAmt.text = "$" + grossSales.toDecimalWithTwoDecimal()
                img.image = UIImage(named: "stockWithHand")
            }
            else if indexPath.row == 1
            {
                lblTitle.text = "Net Profit"
                lblAmt.text = "$" + netProfit.toDecimalWithTwoDecimal()
                img.image = UIImage(named: "dollarWithCircle")
            }
            else if indexPath.row == 2
            {
                lblTitle.text = "Transcations"
                lblAmt.text = "$" + transcations.toDecimalWithTwoDecimal()
                img.image = UIImage(named: "dollarInfinity")
            }
            else if indexPath.row == 3
            {
                lblTitle.text = "Active Inventory"
                lblAmt.text = activeInventory
                img.image = UIImage(named: "activeInventory")
            }
            else if indexPath.row == 4
            {
                lblTitle.text = "Low Inventory"
                lblAmt.text = lowInentory
                img.image = UIImage(named: "lowInventory")
            }
            else if indexPath.row == 5
            {
                lblTitle.text = "Out Of Stock"
                lblAmt.text = outOfStock
                img.image = UIImage(named: "outofstockbox")
            }
        }
        
        
        
        return cell;
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 3 {
            bottomBarViewGVar.btnInventory.sendActions(for: .touchUpInside)
        }
        if indexPath.row == 4 {
            let vc = UIStoryboard.Dashboard.instantiateViewController(withIdentifier: "LowInventoryVC") as! LowInventoryVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 5 {
            let vc = UIStoryboard.Dashboard.instantiateViewController(withIdentifier: "OutOfStockListVC") as! OutOfStockListVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
      
        return CGSize(width: (collectionView.frame.width / 2) - 7.5 , height: 75)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 7.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 15
    }
}

// handle filter for filter
extension DashboardVC:DashboardFilterDelegate
{
    func filterViewDidSelectData(fromDate: String, toDate: String) {
        print(fromDate)
        print(toDate)
        
        filterStartDate = fromDate
        filterEndDate = toDate
        btnFilter.setImage(UIImage(named: "icnFilterFill"), for: .normal)
        callAPIForGetDashboardData(loader: true)
    }
    
    func filterReset() {
        filterStartDate = ""
        filterEndDate = ""
        selectedFilterIndex = -1
        btnFilter.setImage(UIImage(named: "icnFilterUnfill"), for: .normal)
        callAPIForGetDashboardData(loader: true)
    }
    
}
